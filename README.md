# DaVinci Resolve scripts by Andrew Shark
# Mp4 Audio Compounder
This script for Davinci Resolve will simplify the process of importing mp4 files.

The problem with mp4 is different for studio and free version of DaVinci Resolve. The Free version does not support h264 decoding at all, so you need to convert your entire videos to supported codec. And with Studio version, h264 is supported, but aac is still cannot be decoded, so you need to convert the audio to the supported codec. Then you need to import it, then place under the video and link with audio clip.

The hassle of doing this for each mp4 file in each project is annoying. This script is addressed against this problem. You just import the mp4 files as usual, and the script will do all the routine. In the end, you got your working clips in the Media Pool and can start montaging.

## Installation
Place the script to the folder `~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/`.

Alternatively, make there a symlink to the script where you cloned the repository. `ln -s ~/Development/davinci-resolve-scripts/mp4\ audio\ compounder.py ~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/mp4\ audio\ compounder.py`

## Usage
Import the mp4 files as normal. Then select menu Workspace -> Scripts -> mp4 audio compounder.

Because of current limitation of api, I cannot move clips or place them to specified timeline track. <s>So you need to do some things manually. You will get notification asking you to move the mp3 audio clip over the broken aac silent audio (that is under the video clip).</s> In 2.0.0 version, you just need to keep the mouse over the empty space (i.e. not over any timeline item) and the script will do the rest for you (it uses xdotool).

The script then makes the compound clips from them, and places the original mp4 file and the converted mp3 file to the Originals bin in media pool.

Note: you need to setup the python 3.6 for DR to recognise it. See Arch Wiki for details.

Note: Currently it only works with English UI.

# Import Media via Dolphin
This script replaces DR's qt file dialog to the kde Dolphin dialog when importing media.

## Setup
Place the script to the folder `~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/`.

Alternatively, make there a symlink to the script where you cloned the repository. `ln -s ~/Development/davinci-resolve-scripts/import\ media\ via\ dolphin.py ~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/import\ media\ via\ dolphin.py`

Open Keyboard Customization. Remove the Ctrl + I shortcut from File -> Import -> Media. Then add Ctrl + I shortcut in Application -> Workspace -> Scripts -> import media via dolphin.

## Usage
Press Ctrl + I. Select needed media. Selected media will be imported in Media Pool.
