#!/usr/bin/env python3.6

# DaVinci Resolve mp4 audio compounder
# Author: Andrew Shark
# Version: 3.0.1

# Only works with English language interface, because Russian UI returns "Видео и Аудио"

import subprocess
import re
import time
import logging
# import dbus
# TODO make notifications revokable by using dbus

# If run from external location (i.e. not from ~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/), we need to import resolve object
# Note that in that case you also need to use environment variables as described in dr documentation.
try:
    resolve
except:
    import DaVinciResolveScript as dvr_script
    resolve = dvr_script.scriptapp("Resolve")

debug = 0

product = resolve.GetProductName()

if product != "DaVinci Resolve Studio":
    subprocess.run(["kdialog", "--passivepopup", "This script is targeted for DR Studio because DR Free does not support h264. Use mp4 import converter instead."])
    exit(1)

project = resolve.GetProjectManager().GetCurrentProject()
mediaPool = project.GetMediaPool()
rootBin = mediaPool.GetRootFolder()
clips = rootBin.GetClipList()
folders = rootBin.GetSubFolderList()

folderOriginals = None
for folder in folders:
    if folder.GetName() == "Originals":
        folderOriginals = folder
        break

if folderOriginals is None:
    folderOriginals = mediaPool.AddSubFolder(rootBin, "Originals")
    mediaPool.SetCurrentFolder(rootBin)

existingMp4AacClips = []
existingCompoundClipsNames = []
existingCompoundClips = []

# if debug:
#     print("len(clips):", len(clips))

for clip in clips:
    clipProperties = clip.GetClipProperty()
    if clip.GetClipProperty("Type") == "Video + Audio" and clip.GetClipProperty("Audio Codec") == "AAC":
        existingMp4AacClips.append(clip)
    # Deleting temporary timelines in case it was not done by previous script runs
    elif clip.GetClipProperty("Type") == "Timeline" and clip.GetClipProperty("Clip Name") == "Temporary_Timeline":
        mediaPool.DeleteClips([clip])
    elif clip.GetClipProperty("Type") == "Compound":
        existingCompoundClipsNames.append(clip.GetClipProperty("Clip Name"))
        existingCompoundClips.append(clip)

print("---\nStarted")

print("compounds names: ", existingCompoundClipsNames)
print("compounds: ", existingCompoundClips)

delay = 0.1 # pause between presses in xdotool

if debug == 1:
    print("Deleting existing compounds")
    mediaPool.DeleteClips(existingCompoundClips)
    time.sleep(2)

mp4ClipsToHandle = []
for mp4AacClip in existingMp4AacClips:
    if mp4AacClip.GetClipProperty("Clip Name").rstrip(".mp4") not in existingCompoundClipsNames:
        mp4ClipsToHandle.append(mp4AacClip)
    else:
        print("There is already a compound for ", mp4AacClip.GetClipProperty("Clip Name"), "exist, skipping it.")

def IsMouseOverResolve():
    # Because resolve console window may be opened, I need to detect exactly the main window
    # xdotool is currently broken in combining two conditions with --all, see https://unix.stackexchange.com/questions/260842/how-to-combine-conditions-with-xdotool
    # I have to do two separate requests and then see where both ids are found

    class_resolve_windows = subprocess.run("xdotool search --onlyvisible  --class resolve", shell=True,
                                           stdout=subprocess.PIPE, encoding="utf-8").stdout.rstrip().split("\n")
    title_dr_windows = subprocess.run("xdotool search --onlyvisible  --name 'DaVinci Resolve'", shell=True,
                                      stdout=subprocess.PIPE, encoding="utf-8").stdout.rstrip().split("\n")
    intersection = list(set(class_resolve_windows) & set(title_dr_windows))
    if len(intersection) != 1:
        print("Warning: cannot detect main window id")
        resolve_window_id = "-1"
    else:
        resolve_window_id = intersection[0]
    window_id_under_mouse = subprocess.run("xdotool getmouselocation --shell | grep WINDOW | cut -f2 -d '='", shell=True, stdout=subprocess.PIPE, encoding="utf-8").stdout.rstrip()
    logging.debug("id of windows: ", resolve_window_id, window_id_under_mouse)

    if resolve_window_id == window_id_under_mouse:
        return True
    else:
        print("Window ids:", resolve_window_id, window_id_under_mouse)
        return False

def xdotool_procedure(durationDiff):
    # Until dr api limitations are fixed I need to make manupulations in ui with keyboard and mouse input simulation.
    # I am afraid it update will happen maybe 5 years later or so if ever, because as we see, bmd does not like to update versions. At the moment of writing, it is DR v17.4.3
    # See https://forum.blackmagicdesign.com/viewtopic.php?f=12&t=155279

    # I had mp4 files, that extracted audio tracks were both longer and shorter than video track. To avoid the hassle of handling these two situations, I just delete the silent track and place the normal there.
    # So if it the audio is shorter than video, there will no be a short tail of silent track. And if it is longer, I remove the tail from longer track.

    subprocess.run(
        ["kdialog", "--passivepopup", "Please keep the mouse over the empty space of timeline. The script will click there when needed.",
         "--title", "DR mp4 import helper", "--icon", "/usr/share/icons/hicolor/64x64/apps/DV_Resolve.png"])

    # Activate the resolve window
    subprocess.run(["wmctrl", "-x", "-a", "resolve.resolve"])
    time.sleep(0.5)

    # Place playhead to the beginning of timeline
    subprocess.run(["xdotool", "key", "Up", "Up"])
    time.sleep(delay)
    # Unselect all
    subprocess.run(["xdotool", "key", "alt+shift+a"])
    time.sleep(delay)
    # Unlink audio and video
    subprocess.run(["xdotool", "key", "ctrl+alt+l"])
    time.sleep(delay)

    # There is no way I can select audio part clip with hotkey (as far as I can tell). So I do this dancing with locking video track.
    # Really fortunately is that fact, that I have found a way how to do the trick. Because again, I cannot select clips such easily.

    # Lock the v1 track
    subprocess.run(["xdotool", "key", "alt+shift+1"])
    time.sleep(delay)
    # Select the first A+V clip
    subprocess.run(["xdotool", "key", "Up"])
    time.sleep(delay)
    subprocess.run(["xdotool", "key", "shift+v"])
    time.sleep(delay)
    # Delete it. As the video track is locked, only the audio part of clip will be deleted. That part is that silent aac encoded stream.
    subprocess.run(["xdotool", "key", "BackSpace"])
    time.sleep(delay)

    # This trick was hard to guess. It is required to click on empty space in timeline. Without it the following ctrl+a and ctrl+x very often fails.
    # This is davinci resolve's fault, not the xdotool's.
    # Because of this, you need to keep your mouse over empty space of timeline.

    if IsMouseOverResolve():
        subprocess.run(["xdotool", "click", "1"])
        print("Made mouse click")
        time.sleep(delay)
    else:
        print("Warning: Did not click, because mouse not over resolve window")

    # Cut second clip
    subprocess.run(["xdotool", "key", "ctrl+a"])
    time.sleep(delay)
    subprocess.run(["xdotool", "key", "ctrl+x"])
    time.sleep(delay)
    # Paste it in the beginning of timeline
    subprocess.run(["xdotool", "key", "Up", "Up"])
    time.sleep(delay)
    subprocess.run(["xdotool", "key", "ctrl+v"])
    time.sleep(delay)
    # Unlock the v1 track
    subprocess.run(["xdotool", "key", "alt+shift+1"])
    time.sleep(delay)

    if durationDiff < 0:
        print("Cutting the tail of the audio, as it is longer than video.")
        # Cutting the tail in case the audio is longer than video. So we will not have a compound clip with several black frames at the end.
        subprocess.run(["xdotool", "key", "Down", "Down", "Up"])
        time.sleep(delay)
        subprocess.run(["xdotool", "key", "ctrl+b", "shift+v", "BackSpace"])
        time.sleep(delay)
    else:
        print("Not cutting the tail, because video is longer than audio")

def DurationToSeconds(duration):
    # duration in format like: 00:00:43:03
    ftr = [0, 3600, 60, 1]
    return sum([a * b for a, b in zip(ftr, map(int, duration.split(':')))])

for mp4ClipToHandle in mp4ClipsToHandle:
    print("---\nHandling: ", mp4ClipToHandle.GetClipProperty("Clip Name"))

    videoDuration = mp4ClipToHandle.GetClipProperty("Duration")

    fileName = mp4ClipToHandle.GetClipProperty("File Name")
    filePath = mp4ClipToHandle.GetClipProperty("File Path")

    humanName = re.sub('.mp4$', '', fileName)
    audioFilePath = re.sub('mp4$', 'mp3', filePath)

    print("Started ffmpeg process for", fileName)
    subprocess.run(["ffmpeg", "-hide_banner", "-loglevel", "0", "-nostdin",  "-i", filePath, audioFilePath])
    print("Finished ffmpeg process for", fileName)
    # TODO Research how to make the extracted audio to match the video duration

    importedAudioClip = mediaPool.ImportMedia(audioFilePath)[0]

    audioDuration = importedAudioClip.GetClipProperty("Duration")

    durationDiff = 0
    if videoDuration != audioDuration:
        print("Warning: audio and video duration mismatch")

        # # I could set the Out property, so when you append it, it is needed duration, but why Out property is read only?
        # print("out: " + importedAudioClip.GetClipProperty("Out"))
        # print(importedAudioClip.SetClipProperty("Out", videoDuration)) # <- Returns False. Bmd, when you will fix?
        # print("out now: ",importedAudioClip.GetClipProperty("Out"))

        vid_seconds = DurationToSeconds(videoDuration)
        aud_seconds = DurationToSeconds(audioDuration)
        durationDiff = vid_seconds - aud_seconds

        print("Difference:", durationDiff, " video:7", videoDuration, "audio:", audioDuration, )

    temporaryTimeline = mediaPool.CreateEmptyTimeline("Temporary_Timeline")
    videoClipOnTimeline = mediaPool.AppendToTimeline(mp4ClipToHandle)[0]
    audioClipOnTimeline = mediaPool.AppendToTimeline(importedAudioClip)[0]

    # print("clip on timeline", videoClipOnTimeline.GetProperty())

    timelineItems_A1 = []
    # timelineItems_A2 = []
    timelineItems_V1 = []

    retriesLimit = 5
    # subprocess.run(["kdialog", "--passivepopup", "Please drag the normal audio clip to the silent audio clip", str(retriesLimit), "--title", "DR mp4 import helper", "--icon", "/usr/share/icons/hicolor/64x64/apps/DV_Resolve.png"])

    xdotool_procedure(durationDiff)
    print("Finished xdotool procedure, now will check conditions")

    while ( len(timelineItems_A1) != 1 or len(timelineItems_V1) != 1
            or temporaryTimeline.GetTrackCount("audio") != 1
            # Strange magic numbers returned about start point 01:00:00:00 (beginning of timeline) that I saw were 108000 and 216000. Decided to compare against video track instead.
            or timelineItems_A1[0].GetStart() != timelineItems_V1[0].GetStart()
          ) and retriesLimit > 0:

        timelineItems_A1 = temporaryTimeline.GetItemListInTrack("audio", 1)
        timelineItems_V1 = temporaryTimeline.GetItemListInTrack("video", 1)

        if temporaryTimeline.GetTrackCount("audio") > 1:
            print("Please drag the normal audio clip to the proper place and delete extra audio tracks", retriesLimit)
        else:
            print("Please drag the normal audio clip to the place of silent audio clip", retriesLimit)
        retriesLimit -= 1
        time.sleep(1)

    if retriesLimit <= 0:
        print("Waiting timeout, exitting")
        subprocess.run(["kdialog", "--passivepopup", "Waiting timeout, restart the script if needed."])
        exit(1)
    else:
        logging.info("Thanks, continuing")

    # For some weird reason, if I use this, then in the next import the video is imported only as audio (that silent track).
    # Why it is so buggy? I will give up for now.

    # retriesLimit = 15
    # while timelineItems_A1[0].GetEnd() != timelineItems_V1[0].GetEnd() and retriesLimit > 0:
    #     print("Please align the tracks by trimming", retriesLimit)
    #     subprocess.run(["kdialog", "--passivepopup", "Please align the tracks by trimming"])
    #     retriesLimit -= 1
    #     time.sleep(1)
    #
    # if retriesLimit <= 0:
    #     print("Waiting timeout, exitting")
    #     subprocess.run(["kdialog", "--passivepopup", "Waiting timeout, restart the script if needed."])
    #     exit(1)
    # else:
    #     print("Thanks, continuing 2")

    temporaryTimeline.CreateCompoundClip(timelineItems_A1 + timelineItems_V1, {"name": humanName})

    mediaPool.DeleteTimelines([temporaryTimeline])
    if debug != 1:
        mediaPool.MoveClips([mp4ClipToHandle, importedAudioClip], folderOriginals)

print("---\nFinished")
