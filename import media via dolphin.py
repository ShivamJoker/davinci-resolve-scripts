#!/usr/bin/env python3

# DaVinci Import media via Dolphin
# Author: Andrew Shark

import subprocess

# If run from external location (i.e. not from ~/.local/share/DaVinciResolve/Fusion/Scripts/Utility/), we need to import resolve object
# Note that in that case you also need to use environment variables as described in dr documentation.
try:
    resolve
except:
    import DaVinciResolveScript as dvr_script
    resolve = dvr_script.scriptapp("Resolve")

print("Started")

mediaPool = resolve.GetProjectManager().GetCurrentProject().GetMediaPool()

# p = subprocess.run(["kdialog", "--getopenfilename", "--multiple", "--separate-output"], capture_output=True)
# p = subprocess.run("kdialog --getopenfilename --multiple --separate-output", capture_output=True, shell=True)

# For some reason calling kdialog from DR on wayland causes kdialog's crash, while calling it from pycharm (like dr, it is x11 app) does not cause crash.
# Calling kdialog from DR in X11 works as expected.
# As a workaround, I call kdialog window with QT_QPA_PLATFORM=xcb variable.
# TODO: is there a bug report for this?
p = subprocess.run("QT_QPA_PLATFORM=xcb kdialog --getopenfilename --multiple --separate-output ~/Videos", capture_output=True, shell=True)

print(p.stderr.decode())
files_to_import = p.stdout.decode().rstrip().split("\n")
print("Importing files:\n" + "\n".join(files_to_import))
mediaPool.ImportMedia(files_to_import)
print("Finished")
